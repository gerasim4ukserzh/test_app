from django.conf.urls import url
from api.api.pages import page_list_view, page_retrieve_view

urlpatterns = [
    url(r'^pages/$', page_list_view, name='page-list'),
    url(r'^pages/(?P<page_id>[\d\-]+)/$', page_retrieve_view, name='page-retrieve')
]
