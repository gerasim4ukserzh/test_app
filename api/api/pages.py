
from rest_framework import generics, status
from rest_framework.response import Response

from app.models import Page
from api.serializers.pages import PageListSerializer, PageSerializer


class PageList(generics.ListAPIView):
    queryset = Page.objects.all()
    serializer_class = PageListSerializer


class PageRetrieve(generics.RetrieveAPIView):
    queryset = Page.objects.none()
    serializer_class = PageSerializer

    def retrieve(self, request, *args, **kwargs):
        page_id = kwargs.get('page_id')
        obj = Page.objects.filter(pk=page_id).first() if page_id else None
        if obj:
            serializer = self.get_serializer(obj)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


page_list_view = PageList.as_view()
page_retrieve_view = PageRetrieve.as_view()

