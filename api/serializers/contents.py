from django.db.models import F
from django.conf import settings
from rest_framework import serializers
from app.models import ContentText, ContentAudio, ContentVideo, BaseContent


class BaseContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseContent
        fields = ['id', 'title', 'counter']


class ContentVideoSerializer(BaseContentSerializer):
    class Meta:
        model = ContentVideo
        fields = BaseContentSerializer.Meta.fields + ['video_link', 'subtitles_link']


class ContentAudioSerializer(BaseContentSerializer):
    class Meta:
        model = ContentAudio
        fields = BaseContentSerializer.Meta.fields + ['bitrate']


class ContentTextSerializer(BaseContentSerializer):
    class Meta:
        model = ContentText
        fields = BaseContentSerializer.Meta.fields + ['text']


class ContentSerializer(object):
    @classmethod
    def get_page_contents(cls, page):
        result = []
        cls._add_data(ContentText, page, ContentTextSerializer, result)
        cls._add_data(ContentVideo, page, ContentVideoSerializer, result)
        cls._add_data(ContentAudio, page, ContentAudioSerializer, result)
        return result

    @classmethod
    def _add_data(cls, model, page, serializer, result):
        lst = model.objects.filter(page=page).all()
        for obj in lst:
            result.append(serializer(obj).data)
            # if not settings.IS_TEST:
            #     cls._inc_counter(obj)
        return result

    @classmethod
    def _inc_counter(cls, obj):
        # TODO: вынести в celery
        obj.counter = F('counter') + 1
        obj.save()
