from rest_framework import serializers
from app.models import Page
from .contents import ContentSerializer


class PageSerializer(serializers.ModelSerializer):
    contents = serializers.SerializerMethodField(read_only=True)

    def get_contents(self, page):
        return ContentSerializer.get_page_contents(page)

    class Meta:
        model = Page
        fields = ['id', 'title', 'contents']


class PageListSerializer(serializers.ModelSerializer):
    detail = serializers.SerializerMethodField()

    def get_detail(self, page):
        result = None
        request = self.context.get('request')
        if request:
            uri = request.build_absolute_uri()
            if uri:
                uri = uri.split('?')[0]
                result = '{}{}/'.format(uri, page.id)
        return result

    class Meta:
        model = Page
        fields = ['id', 'title', 'detail']
