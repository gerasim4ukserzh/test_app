from django.test import TestCase, Client, override_settings
from django.urls import reverse

from rest_framework import status

from app.models import Page, ContentVideo, ContentText, ContentAudio
from api.serializers.pages import PageSerializer, PageListSerializer

client = Client()


@override_settings(IS_TEST=True)
class GetPagesTest(TestCase):

    def setUp(self):
        for x in range(1, 3):
            page = Page.objects.create(title='Page {}'.format(x))
            for n in range(1, 3):
                ContentText.objects.create(title='Text {}'.format(n), page=page, text='Big text {}'.format(n))
                ContentAudio.objects.create(title='Audio {}'.format(n), page=page, bitrate=n)
                ContentVideo.objects.create(title='Video {}'.format(n), page=page, video_link='v_link{}'.format(n),
                                            subtitles_link='s_link{}'.format(n))

    def test_get_all_pages(self):
        response = client.get(reverse('page-list'))
        pages = Page.objects.all()
        serializer = PageListSerializer(pages, many=True, context={'request': response.wsgi_request})
        self.assertEqual(response.data.get('results'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_one_page(self):
        id = 1
        response = client.get(reverse('page-retrieve', kwargs={'page_id': id}))
        page = Page.objects.get(pk=id)
        serializer = PageSerializer(page)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_not_found(self):
        response = client.get(reverse('page-retrieve', kwargs={'page_id': 0}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

