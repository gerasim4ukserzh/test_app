#!/bin/bash

python manage.py migrate
python manage.py initadmin
python manage.py loaddata data

echo Starting server
python manage.py runserver 0:8000