from django.contrib import admin

from .models import Page, ContentVideo, ContentAudio, ContentText


class BaseAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ['^title']


class PageAdmin(BaseAdmin):
    pass


class BaseContentAdmin(BaseAdmin):
    readonly_fields = ('counter', )
    raw_id_fields = ('page', )
    list_display = ('title', 'page')


class ContentVideoAdmin(BaseContentAdmin):
    pass


class ContentAudioAdmin(BaseContentAdmin):
    pass


class ContentTextAdmin(BaseContentAdmin):
    pass


admin.site.register(Page, PageAdmin)
admin.site.register(ContentVideo, ContentVideoAdmin)
admin.site.register(ContentAudio, ContentAudioAdmin)
admin.site.register(ContentText, ContentTextAdmin)
