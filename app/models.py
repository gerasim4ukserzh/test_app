from django.db import models


class BaseTitle(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=255, null=False, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class Page(BaseTitle):
    class Meta:
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'


class BaseContent(BaseTitle):
    counter = models.IntegerField(verbose_name='Счетчик просмотров', default=0)
    page = models.ForeignKey(Page, verbose_name='Страница', on_delete=models.CASCADE)

    class Meta:
        abstract = True


class ContentVideo(BaseContent):
    video_link = models.CharField(verbose_name='Ссылка на видеофайл', max_length=1024)
    subtitles_link = models.CharField(verbose_name='Ссылка на субтитры', max_length=1024)

    class Meta:
        verbose_name = 'Контент - Видео'
        verbose_name_plural = 'Контент - Видео'


class ContentAudio(BaseContent):
    bitrate = models.PositiveIntegerField(verbose_name='Битрейт')

    class Meta:
        verbose_name = 'Контент - Аудио'
        verbose_name_plural = 'Контент - Аудио'


class ContentText(BaseContent):
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Контент - Текст'
        verbose_name_plural = 'Контент - Тексты'
