from django.contrib.auth.models import User
from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        admin = User.objects.filter(username='admin').first()
        if not admin:
            User.objects.create_superuser('admin', 'admin@example.com', 'admin')
