Запуск автотестов `docker‐compose up autotests`

Запуск сервера `docker‐compose up runserver`

При запуске сервера автоматически создается супервользователь **admin** с паролем **admin**.
После запуска админка доступна по адресу http://0.0.0.0:8000/admin

**API**

- _Пример получения списка всех страниц_

`curl -X GET 'http://0.0.0.0:8000/api/pages/?page=2'`

- _Пример получения детальной информации о странице с id=1_

`curl -X GET 'http://0.0.0.0:8000/api/pages/1/'`
